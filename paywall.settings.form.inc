<?php

/**
 * @file
 * Settings form of Paywall module.
 */

/**
 * Settings form.
 */
function paywall_settings_form() {

  $form = array();

  $form['paywall_uid'] = array(
    '#title' => 'UID',
    '#type' => 'textfield',
    '#description' => t('Your account uid in !site', array('!site' => l(t('zhi12.cn'), 'https://www.zhi12.cn', array('attributes' => array('target' => '_blank'))))),
    '#size' => '8',
    '#default_value' => variable_get('paywall_uid', ''),
    '#required' => TRUE,
  );

  $form['paywall_secret'] = array(
    '#title' => t('Secret', array(), array('context' => 'Account Secret')),
    '#type' => 'textfield',
    '#description' => t('Your account secret in !site', array('!site' => l(t('zhi12.cn'), 'https://www.zhi12.cn', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => variable_get('paywall_secret', ''),
    '#required' => TRUE,
  );

  $form['paywall_percent'] = array(
    '#title' => t('Hiding Ratio'),
    '#type' => 'textfield',
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => '%',
    '#description' => t('Hiding Ratio of paywall nodes, this setting will be ignored if use [$][/$] tags to set paywall area in node manually.'),
    '#default_value' => variable_get('paywall_percent', '50'),
    '#required' => TRUE,
    '#element_validate' => array('_paywall_percent_validate'),
    '#attributes' => array(
      // Lead with a blank to hack to output a number type input.
      ' type' => 'number',
      'min' => 10,
      'max' => 90,
      'step' => 10
    ),
  );

  $form['paywall_overlay'] = array(
    '#title' => t('Enable Popup（Experimental）'),
    '#type' => 'checkbox',
    '#description' => t('Open paywall links in popup window for better UX'),
    '#default_value' => variable_get('paywall_overlay', FALSE),
  );

  $form['advanced'] = array(
    '#title' => t('Advanced'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['paywall_gateway'] = array(
    '#title' => t('Remote Gateway'),
    '#type' => 'textfield',
    '#description' => t('Gateway URL of remote server. Default value: @url', array('@url' => 'https://www.zhi12.cn')),
    '#default_value' => variable_get('paywall_gateway', 'https://www.zhi12.cn'),
  );

  return system_settings_form($form);
}


/**
 * Paywall percent validate.
 */
function _paywall_percent_validate($element, &$form_state, $form) {
  $percents = array(10, 20, 30, 40, 50, 60, 70, 80, 90);
  if (!in_array($element['#value'], $percents)) {
    form_set_error('paywall_percent', t('@title invalid', array('@title' => $element['#title'])));
  }
}