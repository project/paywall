<?php

/**
 * @file
 * @author TheRiseIt, Inc. <contact@theriseit.com>
 */

/**
 * Convert paywall tags into paywall components.
 */
function paywall_content_convert($content, $data, $access = FALSE) {

  if (!function_exists('_filter_htmlcorrector')) {
    require_once 'htmlcorrect.inc.php';
  }

  // Count paywall tag pairs.
  $pattern = '/\[\$\].+?\[\/\$\]/si';
  $matches = array();
  preg_match_all($pattern, $content, $matches);
  $paywall_count = count($matches[0]);

  // Access allowed.
  if ($access) {
    if ($paywall_count < 1) {
      // Add access allowed message before content.
      $content_origin = $content;
      $output_amount  = isset($data['amount']) ? t('（@amount元）', array('@amount' => $data['amount'])) : '';
      $content        = '<div class="paywall paywall-mb"><div class="paywall-center">
        <small class="paywall-highlight">' . t('* 文本为付费内容@output_amount，您已获得全文阅读权限', array('@output_amount' => $output_amount), array('langcode' => 'zh-hans')) . '</small>
        </div></div>' . $content_origin;
    }
    else {
      // Add access allowed message before paywall area.
      $pattern = '/\[\$\](.+?)\[\/\$\]/si';
      $text = t('* 本部分为付费内容，您已获得阅读权限', array(), array('langcode' => 'zh-hans'));
      $replacement = '<div id="paywall" class="paywall"><div class="paywall-center"><small class="paywall-highlight">' . $text . '</small></div>$1</div>';
      $content = preg_replace($pattern, $replacement, $content, 1);
      $replacement = '<div class="paywall"><div class="paywall-center"><small class="paywall-highlight">' . $text . '</small></div>$1</div>';
      $content = preg_replace($pattern, $replacement, $content);
    }

    return $content;
  }
  // Access denied.
  else {

    $percent = isset($data['percent']) ? $data['percent'] : 50;
    $form    = isset($data['form']) ? $data['form'] : NULL;

    // Hide 50% content if no paywall tags.
    if ($paywall_count < 1) {

      $func_strlen  = function_exists('mb_strlen') ? 'mb_strlen' : 'strlen';
      $str_length   = $func_strlen($content);
      $show_percent = (100 - $data['percent']) / 100;
      $show_length  = round($str_length * $show_percent);
      $func_substr  = function_exists('mb_substr') ? 'mb_substr' : 'substr';
      $_show_part   = $func_substr($content, 0, $show_length);
      $show_part    = _filter_htmlcorrector($_show_part);

      // Relace second half with paywall code form.
      $content = $show_part;
      $content .= '
        <div class="paywall-box paywall-center">
          <div class="paywall-mask"></div>
          <div class="paywall-lock"><span class="icon-lock-m"></span></div>
            <div class="paywall paywall-reset"><div class="paywall-highlight">' . t('剩余@percent%内容付费后可查看', array('@percent' => $percent), array('langcode' => 'zh-hanss')) . '</div>' .
            $form .
         '</div></div>';
    }
    else {
      // Convert paywall tags to paywall.
      foreach ($matches[0] as $k => $m) {

        // First tags convert to form, rest to message.
        if ($k == 0) {
          $replace = '<div class="paywall paywall-center">' . $form . '</div>';
        }
        else {
          $replace = '<div class="paywall paywall-center">
                        <div style="padding: 40px 0px;"><strong>' . t('本部分为付费内容，支付后可查看', array(), array('langcode' => 'zh-hans')) . '</strong></div>
                      </div>';
        }

        $content = str_replace_once($m, $replace, $content);
      }
    }
  }

  return $content;
}

/**
 * @see https://www.php.net/manual/zh/function.str-replace.php#86177
 */
function str_replace_once($search, $replace, $subject) {
  $first_char = strpos($subject, $search);
  if ($first_char !== FALSE) {
    $before_str = substr($subject, 0, $first_char);
    $after_str = substr($subject, $first_char + strlen($search));
    return $before_str . $replace . $after_str;
  }
  else {
    return $subject;
  }
}

/**
 * Check code valid via remote server.
 */
function paywall_code_check($data) {

  $query = http_build_query($data);

  $url = paywall_base_url() . '/paywall/api/codecheck?' . $query;
  $status = file_get_contents($url);

  return $status;
}

/**
 * Get client ip.
 */
function paywall_get_client_ip() {
  if (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
    $ip = getenv('REMOTE_ADDR');
  }
  elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  elseif (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
    $ip = getenv('HTTP_CLIENT_IP');
  }
  elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
    $ip = getenv('HTTP_X_FORWARDED_FOR');
  }
  return preg_match('/[\d\.]{7,15}/', $ip, $matches) ? $matches[0] : '';
}

/**
 * Generate sign.
 */
function paywall_get_sign(array $data, $secret) {

  // Remove unused data.
  if (isset($data['q'])) {
    unset($data['q']);
  }
  if (isset($data['sign'])) {
    unset($data['sign']);
  }

  // Add secret.
  $data['secret'] = $secret;

  // Sort.
  ksort($data);

  $str = http_build_query($data);

  // Sign.
  $sign = md5(md5($str));

  return $sign;
}

/**
 * Add sign to an array.
 *
 * @return Array $data with an added 'sign'.
 */
function paywall_sign(&$data, $secret) {
  $sign = paywall_get_sign($data, $secret);
  $data['sign'] = $sign;
  return $data;
}

/**
 * Sign Check.
 *
 * @return Boolean TRUE for check passed, FALSE for failed.
 */
function _paywall_sign_check($data, $secret) {
  if (!isset($data['sign'])) {
    return FALSE;
  }

  $sign = paywall_get_sign($data, $secret);
  return $sign == $_GET['sign'];
}
