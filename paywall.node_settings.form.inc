<?php

/**
 * @file
 * Form for set price to a node.
 */

/**
 * Paywall settings form for node.
 */
function paywall_node_settings_form($form, $form_state, $node) {

  $form = array();

  $settings = paywall_get_node_settings($node);

  $form['charge'] = array(
    '#title' => t('Paywall Enabled', array(), array('context' => 'Node setting')),
    '#type' => 'radios',
    '#options' => array(
      '0' => t('No', array(), array('context' => 'Paywall charge mode')),
      '1' => t('Yes', array(), array('context' => 'Paywall charge mode')),
    ),
    '#description' => t('Choose YES to enable paywall in this node'),
    '#required' => TRUE,
    '#default_value' => $settings ? 1 : 0,
  );

  $form['amount'] = array(
    '#title' => t('Price', array(), array('context' => 'Paywall price')),
    '#description' => t('You can use <code>[$][/$]</code> tags to set paywall area manually, by default content will hide by !link', array('!link' => l(t('Hiding Ratio'), 'admin/config/paywall'))),
    '#type' => 'textfield',
    '#default_value' => isset($settings->amount) ? floatval($settings->amount) : 1,
    '#field_suffix' => t('Yuan'),
    '#size' => 6,
    '#attributes' => array(
      ' type' => 'number',
      'min' => 1,
      'max' => 9999,
    ),
    '#states' => array(
      'invisible' => array(
        ':input[name="charge"]' => array('value' => 0),
      ),
    ),
  );

  $form['#node'] = $node;

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validate.
 */
function paywall_node_settings_form_validate($form, &$form_state) {

  global $user;

  $amount = $form_state['values']['amount'];

  // If node not free, then validate amount value.
  if ($form_state['values']['charge'] == 1) {
    if (!is_numeric($amount) || $amount < 1 || $amount > 9999) {
      form_set_error('amount', t('Set a price between 1~9999'));
    }
  }

}

/**
 * Form submit.
 */
function paywall_node_settings_form_submit($form, $form_state) {
  $values = $form_state['values'];
  $node = $form['#node'];
  paywall_node_settings_save($node, $values['charge'], $values['amount']);
  drupal_set_message(t('Paywall settings saved !'));
  drupal_goto('node/' . $node->nid);
}
